package com.volvo.congestiontaxcalculator;

import com.volvo.congestiontaxcalculator.transport.Motorbike;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RunWith(SpringRunner.class)
public class CalculatorTest {

    @Test
    public void test1() throws ParseException {
        CongestionTaxCalculator taxCalculator = new CongestionTaxCalculator();
        Motorbike motorbike = new Motorbike();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date[] testDates = new Date[]{
                formatter.parse("2013-02-07 06:29:00"),
                formatter.parse("2013-02-07 06:58:00"),
                formatter.parse("2013-02-07 07:50:00"),
        };

        Assert.assertEquals(18, taxCalculator.getTax(motorbike, testDates));
    }

    @Test
    public void test2() throws ParseException {
        CongestionTaxCalculator taxCalculator = new CongestionTaxCalculator();
        Motorbike motorbike = new Motorbike();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date[] testDates = new Date[]{
                formatter.parse("2013-01-07 18:45:00"),
                formatter.parse("2013-07-07 06:58:27"),
        };

        Assert.assertEquals(0, taxCalculator.getTax(motorbike, testDates));
    }
}
