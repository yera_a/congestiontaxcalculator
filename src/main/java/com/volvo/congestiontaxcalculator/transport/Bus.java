package com.volvo.congestiontaxcalculator.transport;


import com.volvo.congestiontaxcalculator.VehicleType;

public class Bus implements Vehicle{
    @Override
    public VehicleType getVehicleType() {
        return VehicleType.Bus;
    }
}
