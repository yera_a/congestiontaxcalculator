package com.volvo.congestiontaxcalculator.transport;

import com.volvo.congestiontaxcalculator.VehicleType;

public class Motorcycle implements Vehicle {
    @Override
    public VehicleType getVehicleType() {
        return VehicleType.Motorcycle;
    }
}
