package com.volvo.congestiontaxcalculator.transport;

import com.volvo.congestiontaxcalculator.VehicleType;

public class Motorbike implements Vehicle {
    @Override
    public VehicleType getVehicleType() {
        return VehicleType.Motorbike;
    }
}
