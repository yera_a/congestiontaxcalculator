package com.volvo.congestiontaxcalculator.transport;


import com.volvo.congestiontaxcalculator.VehicleType;

public class Car implements Vehicle {

    public VehicleType getVehicleType() {
        return VehicleType.Car;
    }
}
