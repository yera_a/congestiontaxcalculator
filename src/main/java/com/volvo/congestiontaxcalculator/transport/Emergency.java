package com.volvo.congestiontaxcalculator.transport;


import com.volvo.congestiontaxcalculator.VehicleType;

public class Emergency implements Vehicle{
    @Override
    public VehicleType getVehicleType() {
        return VehicleType.Emergency;
    }
}
