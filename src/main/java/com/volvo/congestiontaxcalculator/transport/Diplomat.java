package com.volvo.congestiontaxcalculator.transport;


import com.volvo.congestiontaxcalculator.VehicleType;

public class Diplomat implements Vehicle{
    @Override
    public VehicleType getVehicleType() {
        return VehicleType.Diplomat;
    }
}
