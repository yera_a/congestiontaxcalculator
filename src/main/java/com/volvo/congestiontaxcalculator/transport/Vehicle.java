package com.volvo.congestiontaxcalculator.transport;

import com.volvo.congestiontaxcalculator.VehicleType;

public interface Vehicle {
    VehicleType getVehicleType();
}
