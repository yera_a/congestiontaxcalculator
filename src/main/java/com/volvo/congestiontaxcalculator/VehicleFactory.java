package com.volvo.congestiontaxcalculator;


import com.volvo.congestiontaxcalculator.transport.*;
import org.springframework.stereotype.Component;

@Component
public class VehicleFactory {

    public Vehicle create(String type) {
        VehicleType vehicleType = VehicleType.valueOf(type);
        switch (vehicleType) {
            case Car:
                return new Car();
            case Emergency:
                return new Emergency();
            case Motorbike:
                return new Motorbike();
            case Bus:
                return new Bus();
            case Diplomat:
                return new Diplomat();
            case Motorcycle:
                return new Motorcycle();
            default:
                throw new IllegalArgumentException("No such vehicle type");
        }
    }
}
