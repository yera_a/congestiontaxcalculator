package com.volvo.congestiontaxcalculator;


import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@RequiredArgsConstructor
@Controller
@RequestMapping(value = "/api", produces = "application/json")
public class RestController {

    private final CongestionTaxCalculator taxCalculator;
    private final VehicleFactory vehicleFactory;

    @GetMapping("/calculate")
    public ResponseEntity<Integer> jobActiveState(@RequestParam String vehicle,
                                                  @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss") Date[] dates) {

        return ResponseEntity.ok(taxCalculator.getTax(vehicleFactory.create(vehicle), dates));
    }

}
