package com.volvo.congestiontaxcalculator;

public enum VehicleType {
    Emergency(true),
    Bus(true),
    Diplomat(true),
    Motorcycle(true),
    Military(true),
    Foreign(true),
    Tractor(true),
    Car(false),
    Motorbike(false);

    final boolean isExempt;

    VehicleType(boolean isExempt){
        this.isExempt = isExempt;
    }
}
